def trump_symbol(trump_color):
  
  if trump_color == 0:
    return "None"
  
  if trump_color == 1:                                          # Red
    return '\x1b[0;31;40m' + "Red" + '\x1b[0m'
  
  if trump_color == 2:                                          # Yellow
    return '\x1b[0;33;40m' + "Yellow" + '\x1b[0m'
  
  if trump_color == 3:                                          # Green
    return '\x1b[0;32;40m' + "Green" + '\x1b[0m'
  
  if trump_color == 4:                                          # Blue
    return '\x1b[0;34;40m' + "Blue" + '\x1b[0m'


def correcting_spaces(trump, trump_card):               # This function returns a string of the correct width to put in front of a trump display string to right-align it.
                                                        # It is needed because "%23s" % string does not work with color codes.
  # Length of 'Trump None (J)' = 14
  # Length of 'Trump None (Final Round)' = 24
  # Length of 'Trump Red (1)' = 13
  # Length of 'Trump Red (11)' = 14
  # Length of 'Trump Red (W)' = 13
  # Length of 'Trump Yellow (1)' = 16
  # Length of 'Trump Yellow (11)' = 17
  # Length of 'Trump Yellow (W)' = 16
  # Length of 'Trump Green (1)' = 15
  # Length of 'Trump Green (11)' = 16
  # Length of 'Trump Green (W)' = 15
  # Length of 'Trump Blue (1)' = 14
  # Length of 'Trump Blue (11)' = 15
  # Length of 'Trump Blue (W)' = 14
  
  if trump_card == None:
    return ""                                   # No correcting spaces needed
  elif trump == 0:
    return "          "                         # 24 - 14 = 10, so 9 whitespaces needed
  elif trump == 1:
    s = "          "                            # 24 - 14 = 10, so 9 whitespaces needed at least
  elif trump == 2:
    s = "       "                               # 24 - 17 = 7, so 6 whitespaces needed at least
  elif trump == 3:
    s = "        "                              # 24 - 16 = 8, so 7 whitespaces needed at least
  elif trump == 4:
    s = "         "                             # 24 - 15 = 9, so 8 whitespaces needed at least
  
  if trump_card.number not in [10, 11, 12, 13]:
    s += " "
  
  return s
  
  
def record_last_trick(player_sequence, card_sequence, round_no, trick_no, trump, trump_card, trick_winner, players):
  
  # This function creates the interface table of the "previous trick" to show to all human players before their turns.
  # The return value of this function is a list of strings, each describing one line of the table.
  # 
  # Desired layout (example situation):
  # 
  # ***************************************************************************
  # * LAST TRICK           Round 7       Trick 4                Trump Red (6) *                 <-- Other trump examples:    Red (W)  or   None (Final Round)  or  None (J)
  # ***************************************************************************
  # * Player                         Card                   Won   Bid  Points *
  # * ----------------------------------------------------------------------- *
  # * Alice                             W  <-- Trick Winner   1     2     120 *
  # * Bob                               6                     1     1     -30 *
  # * Charly                            J                     2     3      90 *
  # ***************************************************************************
  
  last_trick_display = [""]
  last_trick_display.append("***************************************************************************")
  
  s = "* LAST TRICK           Round %-2i      Trick %-2i    " % (round_no, trick_no)
  
  t = "Trump %s" % trump_symbol(trump)
  
  if trump_card != None:
    
    t += " (" + trump_card.symbol() + ")"
    
  else:
    
    t += " (Final Round)"
  
  last_trick_display.append(s + correcting_spaces(trump, trump_card) + t + " *")
  
  last_trick_display.append("***************************************************************************")
  last_trick_display.append("* Player                         Card                   Won   Bid  Points *")
  last_trick_display.append("* ----------------------------------------------------------------------- *")
  
  for i in range(len(players)):
    
    player = players[player_sequence[i]]
    
    s = "* " + "%-30s   " % player.name
    
    if card_sequence[i].number not in [10, 11, 12, 13]:
      s += " "
    
    s += card_sequence[i].symbol()
    
    if player == trick_winner:
      s += "  <-- Trick Winner"
    else:
      s += "                  "
    
    s += "  %2i" % player.tricks + "    %2i" % player.bid + "   %5i" % player.points + " *"
    
    last_trick_display.append(s)
  
  last_trick_display.append("***************************************************************************")
  
  return last_trick_display


def display_last_trick(last_trick_display):
  
  # This function displays the table of the "previous trick", if is not an empty list.
  
  for line in last_trick_display:
    print(line)


def display_trump_choice_phase(round_no, players, mc):
  
  # Desired layout (example situation):
  # 
  # ###########################################################################
  # # CURRENT TRUMP-CHOICE-PHASE                   Round 7                (W) #
  # ###########################################################################
  # # Player                                                           Points #
  # # ----------------------------------------------------------------------- #
  # # Alice                                                               120 #
  # # Bob                                                                  70 #
  # # Charly                                                               90 #
  # ###########################################################################
  # # Hand:       J 9 12 12 1 6 W                                             #
  # ###########################################################################
  # # Trump colors to choose from:       Red    Yellow    Green     Blue      #
  # ###########################################################################
  # # Options:                           1      2         3         4         #
  # ###########################################################################

  print("")
  print("###########################################################################")
  print("# CURRENT TRUMP-CHOICE-PHASE                   Round %-2i               (W) #" % round_no)
  print("###########################################################################")
  print("# Player                                                           Points #")
  print("# ----------------------------------------------------------------------- #")
  for player in players:
    print("# " + "%-30s   " % player.name + "                                 %5i" % player.points + " #")
  print("###########################################################################")
  
  s = "# Hand:      "
  
  hand = mc.hand
  length = 0
  
  for card in hand:
    
    s += " " + card.symbol()
    
    if card.number in [10, 11, 12, 13]:
      length += 3
    else:
      length += 2
  
  for i in range(60 - length):
    s += " "
  
  s += " #"
  
  print(s)
  print("###########################################################################")
  print("# Trump colors to choose from:       " + \
         "\x1b[0;31;40m" + "Red" + "\x1b[0m    \x1b[0;33;40m" + "Yellow" + "\x1b[0m    \x1b[0;32;40m" + "Green" + "\x1b[0m     \x1b[0;34;40m" + "Blue" + "\x1b[0m      #")
  print("###########################################################################")
  print("# Options:                           1      2         3         4         #")
  print("###########################################################################")
  print("")


def display_bidding_phase_for_non_mcs(no_of_players, round_no, round_starter, trump, trump_card, player):
  
  # Desired layout (example situation):
  # 
  # ###########################################################################
  # # CURRENT BIDDING-PHASE               Round 5             Trump Blue (12) #                 <-- Other trump examples:    Red (W)  or   None (Final Round)  or  None (J)
  # ###########################################################################
  # # Player                                                      Bid  Points #
  # # ----------------------------------------------------------------------- #
  # # Alice                                                         4      30 #
  # # Bob                                                                 -40 #
  # # Charly                                                               30 #
  # ###########################################################################
  # # Hand:       J 2 10 11 13                                                #
  # ###########################################################################
  # # Options:    0 1 2 3 4 5                                                 #
  # ###########################################################################
  
  print("")
  print("###########################################################################")
  
  s = "# CURRENT BIDDING-PHASE               Round %-2i   " % round_no
  
  t = "Trump %s" % trump_symbol(trump)
  
  if trump_card != None:
    
    t += " (" + trump_card.symbol() + ")"
    
  else:
    
    t += " (Final Round)"
  
  print(s + correcting_spaces(trump, trump_card) + t + " #")
  
  print("###########################################################################")
  print("# Player                                                      Bid  Points #")
  print("# ----------------------------------------------------------------------- #")
  
  temp_player = round_starter
  
  for p in range(no_of_players):
    
    s = "# " + "%-30s   " % temp_player.name + "                            "
    
    if temp_player.bid != -1:
      
      s += "%2i" % temp_player.bid
      
    else:
      
      s += "  "
    
    print(s + "   %5i" % temp_player.points + " #")
    
    temp_player = temp_player.successor
  
  print("###########################################################################")
  
  s = "# Hand:      "
  
  hand = player.hand
  length = 0
  
  for card in hand:
    
    s += " " + card.symbol()
    
    if card.number in [10, 11, 12, 13]:
      length += 3
    else:
      length += 2
  
  for i in range(60 - length):
    s += " "
  
  s += " #"
  
  print(s)
  print("###########################################################################")
  
  s = "# Options:   "
  
  for i in range(round_no + 1):
    s += " %i" % i
  
  print("%-73s #" % s)
  print("###########################################################################")
  print("")


def display_bidding_phase_for_mc(no_of_players, round_no, round_starter, trump, trump_card, player, sum_of_bids):
  
  # Desired layout (example situation):
  # 
  # ###########################################################################
  # # CURRENT BIDDING-PHASE               Round 7           Trump Yellow (13) #                 <-- Other trump examples:    Red (W)  or   None (Final Round)  or  None (J
  # ###########################################################################
  # # Player                                                      Bid  Points #
  # # ----------------------------------------------------------------------- #
  # # Alice                                                         0     -30 #
  # # Bob                                                           3      20 #
  # # Charly                                                              -30 #
  # ###########################################################################
  # # Hand:       3 7 3 1 3 10 13                                             #
  # ###########################################################################
  # # Options:    0 1 2 3   5 6 7                                             #
  # ###########################################################################
  
  print("")
  print("###########################################################################")
  
  s = "# CURRENT BIDDING-PHASE               Round %-2i   " % round_no
  
  t = "Trump %s" % trump_symbol(trump)
  
  if trump_card != None:
    
    t += " (" + trump_card.symbol() + ")"
    
  else:
    
    t += " (Final Round)"
  
  print(s + correcting_spaces(trump, trump_card) + t + " #")
  
  print("###########################################################################")
  print("# Player                                                      Bid  Points #")
  print("# ----------------------------------------------------------------------- #")
  
  temp_player = round_starter
  
  for p in range(no_of_players):
    
    s = "# " + "%-30s   " % temp_player.name + "                            "
    
    if temp_player.bid != -1:
      
      s += "%2i" % temp_player.bid
      
    else:
      
      s += "  "
    
    print(s + "   %5i" % temp_player.points + " #")
    
    temp_player = temp_player.successor
  
  print("###########################################################################")
  
  s = "# Hand:      "
  
  hand = player.hand
  length = 0
  
  for card in hand:
    
    s += " " + card.symbol()
    
    if card.number in [10, 11, 12, 13]:
      length += 3
    else:
      length += 2
  
  for i in range(60 - length):
    s += " "
  
  s += " #"
  
  print(s)
  print("###########################################################################")
  
  s = "# Options:   "
  
  for i in range(round_no + 1):
    
    if i == round_no - sum_of_bids:
      if i < 10:
        s += "  "
      else:
        s += "   "
    else:
      s += " %i" % i
  
  print("%-73s #" % s)
  print("###########################################################################")
  print("")


def display_trick_taking_phase(no_of_players, round_no, trick_no, trick_starter, trump, trump_card, player, card_sequence, playable_cards):
  
  # Desired layout (example situation):
  # 
  # ###########################################################################
  # # CURRENT TRICK        Round 9       Trick 3            Trump Yellow (11) #                 <-- Other trump examples:    Red (W)  or   None (Final Round)  or  None (J
  # ###########################################################################
  # # Player                         Card                   Won   Bid  Points #
  # # ----------------------------------------------------------------------- #
  # # Alice                             2                     2     2     -10 #
  # # Bob                                                     0     4     -30 #
  # # Charly                                                  0     0     -80 #
  # ###########################################################################
  # # Hand:       13  2  9 11 10 13  W                                        #
  # ###########################################################################
  # # Options:     1                 2                                        #
  # ###########################################################################
  
  print("")
  print("###########################################################################")
  
  s = "# CURRENT TRICK        Round %-2i      Trick %-2i    " % (round_no, trick_no)
  
  t = "Trump %s" % trump_symbol(trump)
  
  if trump_card != None:
    
    t += " (" + trump_card.symbol() + ")"
    
  else:
    
    t += " (Final Round)"
  
  print(s + correcting_spaces(trump, trump_card) + t + " #")
  
  print("###########################################################################")
  print("# Player                         Card                   Won   Bid  Points #")
  print("# ----------------------------------------------------------------------- #")
  
  temp_player = trick_starter
  
  for p in range(no_of_players):
    
    s = "# " + "%-30s   " % temp_player.name
    
    if p < len(card_sequence):
      
      if card_sequence[p].number not in [10, 11, 12, 13]:
        s += " "
      
      s += card_sequence[p].symbol()
      
    else:
      
      s += "  "
    
    s += "                    %2i" % temp_player.tricks + "    %2i" % temp_player.bid + "   %5i" % temp_player.points + " #"
    
    print(s)
    
    temp_player = temp_player.successor
  
  print("###########################################################################")
  
  s = "# Hand:      "
  
  hand = player.hand
  length = 0
  options_string = "# Options:   "
  no_of_options = 0
  
  for card in hand:
    
    if card.number not in [10, 11, 12, 13]:
      s += " "
    
    s += " " + card.symbol()
    
    length += 3
    
    if card in playable_cards:
      no_of_options += 1
      options_string += " %2i" % no_of_options
    else:
      options_string += "   "
  
  for i in range(60 - length):
    s += " "
  
  s += " #"
  
  print(s)
  print("###########################################################################")
  print("%-73s #" % options_string)
  print("###########################################################################")
  print("")
