Project Goals
-------------

The goal of this project is to create machine learning agents for the popular card game "Wizard".

<img src="presentation/images/amigo.jpg" height="100"> <img src="presentation/images/FeedForwardNN.png" height="100">

A detailed course of action and first results can be seen in the comprehensive [presentation](presentation/ResultsPresentation.pdf).

<img src="presentation/images/NNE_1000_success_rate.png" height="100"> <img src="presentation/images/NNE_22_avg_points.png" height="100">



Features
--------

The following features are implemented:

* Playing a game with human players, static algorithms or machine learning agents all at the same time
* Comparing the strength of different static algorithms or machine learning agents in a series of games by creating rating-lists
* Creating learning data for machine learning in single games or using a 'research'-function to create data from a whole series
* Teaching neural networks with previously created learning data (the output is a ready-made machine learning agent)
* Evolving neural networks in a series of data-creation and learning cycles



System Requirements
-------------------

For the machine learning part, in addition to python3, the following software packages are required:

* numpy
* pandas
* scikit-learn
* tensorflow
* keras



Commands
--------

To play a simple game against two static algorithms, for example, use the following command:

`python3 wizard.py p r d 0 3 h My_name s tendency_bot_alpha_6.py s tendency_bot_alpha_8.py`


To play a simple game against a static algorithm and a neural network, use the following exemplary command:

`python3 wizard.py p r d 0 3 h My_name s tendency_bot_alpha_8.py l neural_bid_bot.py NN_7_32k`


A detailed description of all commands can be found in [Synopsis.txt](Synopsis.txt).
