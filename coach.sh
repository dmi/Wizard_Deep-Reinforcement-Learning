#!/bin/sh

clear; clear; clear

rm coached_learning_data.csv

research_games=3000
compare_games=100

python3 wizard.py research rated not_fixed $research_games coached_learning_data.csv 3 4 \
s static_algorithms/tendency_bot_alpha_8.py \
s static_algorithms/uniform_random_bot.py \
s static_algorithms/greedy_bot.py \
s static_algorithms/tendency_bot_alpha_6.py && \
python3 wizard.py teach coached_learning_data.csv $1

rm rating_list.txt ; rm rating_list_s*

python3 wizard.py compare $compare_games 3 \
s static_algorithms/tendency_bot_alpha_8.py \
s static_algorithms/greedy_bot.py \
l ml_algorithms/neural_bid_bot.py $1 && \
cat rating_list_sorted_by_avg_points.txt
