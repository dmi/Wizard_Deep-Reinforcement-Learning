from sys             import *

from data            import *
from human_interface import *
from random          import shuffle



class Cgame:

  def __init__(self):

    self.id            = None                     # (Optional)
    self.no_of_players = None
    self.players       = None
    self.rounds        = []
    self.final_points  = []
    self.data          = []


class Ctrick:

  def __init__(self):

    self.player_sequence = None
    self.card_sequence   = None
    self.winner          = None


class Cplayer:

  def __init__(self, type, name, create_virtual_game, choose_trump, choose_bid, choose_card_to_play):

    self.type = type                                            # Basic variables
    self.name = name

    self.create_virtual_game = create_virtual_game              # Functions (function-pointers) for static algorithms and learning agents only
    self.choose_trump        = choose_trump
    self.choose_bid          = choose_bid
    self.choose_card_to_play = choose_card_to_play

    self.points = 0                                             # Game variables
    self.hand   = []
    self.bid    = -1
    self.tricks = 0

    self.cards_left   = []                                      # Tracking variables for learning data creation only
    self.jesters_left = 0
    self.wizards_left = 0

    # These attributes will be changed in Cplayer instances at runtime:

    self.successor         = None      # (Pointing to the left-hand player at the table)
    self.virtual_game      = None      # (For static algorithms and learning agents only)
    self.rating_list_entry = None      # (For rated games only)


class Ccard:

  def __init__(self, color, number):

    self.color  = color				# Colors:  0 for white cards (wizards and jesters), 1 for red, 2 for yellow, 3 for green, 4 for blue
    self.number = number			# Numbers: 0 for jesters, 14 for wizards, other numbers mapped by identity (1 to 13)


  def symbol(self):                                     # Returns the correct color and symbol of the card

    output_string = str(self.number)

    if output_string == "0":
      output_string = "J"

    if output_string == "14":
      output_string = "W"

    if self.color == 0:                                         # White
      return output_string

    if self.color == 1:                                         # Red
      return '\x1b[0;31;40m' + output_string + '\x1b[0m'

    if self.color == 2:                                         # Yellow
      return '\x1b[0;33;40m' + output_string + '\x1b[0m'

    if self.color == 3:                                         # Green
      return '\x1b[0;32;40m' + output_string + '\x1b[0m'

    if self.color == 4:                                         # Blue
      return '\x1b[0;34;40m' + output_string + '\x1b[0m'


def display_cards(cards):               # Shows all cards of the list 'cards' in its real colors and with the correct symbol

  s = ""

  for c in cards:
    s += c.symbol() + " "

  if len(cards) != 0:
    s = s[:-1]

  print(s)


def sort_cards(cards):                  # Sorts a list of cards starting with the lowest cards, i.e. jesters < colorful cards < wizards

  jesters      = []
  red_cards    = []
  yellow_cards = []
  green_cards  = []
  blue_cards   = []
  wizards      = []

  for c in cards:                       # Create a division of the cards into jesters, red cards, yellow cards, green cards, blue cards and wizards

    if c.number == 0:
      jesters.append(c)

    if c.color == 1:
      red_cards.append(c)

    if c.color == 2:
      yellow_cards.append(c)

    if c.color == 3:
      green_cards.append(c)

    if c.color == 4:
      blue_cards.append(c)

    if c.number == 14:
      wizards.append(c)

  del cards[:]

  cards.extend(jesters)                                                         #  Merge the sorted divisions to form the complete sorted hand
  cards.extend(sorted(red_cards,    key=lambda Ccard : Ccard.number))
  cards.extend(sorted(yellow_cards, key=lambda Ccard : Ccard.number))
  cards.extend(sorted(green_cards,  key=lambda Ccard : Ccard.number))
  cards.extend(sorted(blue_cards,   key=lambda Ccard : Ccard.number))
  cards.extend(wizards)


def play(game_no, create_learning_data, no_of_players, players):                # Plays a game

  print("")
  print("Starting a new game:")
  print("Game number: %s" % game_no)
  print("Create learning data? - %s" % create_learning_data)
  print("Number of players? - %s" % no_of_players)
  print("Players:")
  
  for p in players:
    print(p.type, p.name)


  # Initialization:

  human_players_participating = False

  for p in range(no_of_players):

    player = players[p]

    player.points = 0

    if player.type == "h":

      human_players_participating = True
      last_trick_display = []

    elif player.type == "s":

      player.virtual_game = player.create_virtual_game(no_of_players, p)

    elif player.type == "l":

      player.virtual_game = player.create_virtual_game(no_of_players, p)


  # Set up links to successor-players (forming a circle):

  for n in range(no_of_players - 1):
    players[n].successor = players[n + 1]

  players[-1].successor = players[0]


  # Set up starting players:

  mc_index = no_of_players - 1                  # mc = master of ceremony, i.e. the player who shuffles and distributes the cards and sets the trump color
                                                # May the last player be the MC of round 1!
  mc = players[mc_index]

  round_starter = players[0]                    # Player 0 will start the first trick of the round 1


  # Declare variables which will be reused during loops:

  deck = []                                     #

  bid_sequence = []                             #

  playable_cards = []                           # Used during the trick-taking-phase

  tricks = []

  game               = Cgame()
  game.id            = game_no
  game.no_of_players = no_of_players
  game.players       = list(players)

  player_sequence = []                          # Used during tricks to store who played what
  card_sequence   = []                          #


  # Create the game cards:

  all_cards = []

  for color in range(1, 5):

    all_cards.append(Ccard(0,0))                # Create a jester
    all_cards.append(Ccard(0,14))               # Create a wizard

    for number in range(1, 14):
      all_cards.append(Ccard(color, number))    # Create a numbered card in a certain color


  # Loop through the rounds:

  no_of_rounds = int(60 / no_of_players)

  for round_no in range(1, no_of_rounds + 1):


    # Initialization:

    del bid_sequence[:]

    for player in players:

      player.bid = -1
      player.tricks = 0
      del player.hand[:]

    del deck[:]
    deck = list(all_cards)

    if create_learning_data:

      game_round               = Cround()                               # The variable is named "game_round" instead of "round" to avoid the already existing round-function.
      game.rounds.append(game_round)
      game_round.mc            = mc_index
      game_round.round_starter = (mc_index + 1) % no_of_players
      game_round.hands         = []
      game_round.bids          = []

      tricks = []
      game_round.tricks = tricks

      game_round.won_tricks = []
      game_round.gained_points = []

    else:

      del tricks[:]


    # Distribution of the cards:

    shuffle(deck)

    for player in players:

      hand = player.hand

      for card_number in range(1, round_no + 1):

        hand.append(deck.pop())

      sort_cards(hand)

      #print("")
      #print("%s received the following cards:" % player.name)
      #display_cards(hand)


    # Decision of the trump color:

    if len(deck) == 0:

      trump = 0
      trump_card = None

    else:

      trump_card = deck.pop()
      trump = trump_card.color

      if ((trump == 0) and (trump_card.number == 14)):			                                # Does the MC have to decide the trump color?

        if mc.type == "h":

          while trump not in [1, 2, 3, 4]:

            display_last_trick(last_trick_display)
            display_trump_choice_phase(round_no, players, mc)

            try:
              trump = int(input("%s, please enter your choice: " % mc.name))
            except ValueError:
              trump = -1

        elif mc.type == "s":

          trump = mc.choose_trump(mc.virtual_game, no_of_players, round_no, mc.hand)

          if trump not in [1, 2, 3, 4]:

            print("ERROR: The static algorithm %s returned an inadmissible value." % mc.name)
            sys.exit(2)

        elif mc.type == "l":

          trump = mc.choose_trump(mc.virtual_game, no_of_players, round_no, mc.hand)

          if trump not in [1, 2, 3, 4]:

            print("ERROR: The learning agent %s returned an inadmissible value." % mc.name)
            sys.exit(2)


    # Bidding-phase:

    player = round_starter

    sum_of_bids = 0

    while player.successor != round_starter:                            # Loop through the players except for the MC, who is a special case, which is handled after the loop.

      if player.type == "h":

        bid = -1

        while bid not in range(round_no + 1):

          display_last_trick(last_trick_display)
          display_bidding_phase_for_non_mcs(no_of_players, round_no, round_starter, trump, trump_card, player)

          try:
            bid = int(input("%s, please enter your choice: " % player.name))
          except ValueError:
            bid = -1

      elif player.type == "s":

        bid = player.choose_bid(player.virtual_game, no_of_players, round_no, trump, trump_card, bid_sequence, player.hand, players, all_cards)

        if bid not in range(round_no + 1):

          print("ERROR: The static algorithm %s returned an inadmissible value ('%s')." % (player.name, bid))
          sys.exit(2)

      elif player.type == "l":

        bid = player.choose_bid(player.virtual_game, no_of_players, round_no, trump, trump_card, bid_sequence, player.hand, players, all_cards)

        if bid not in range(round_no + 1):

          print("ERROR: The learning agent %s returned an inadmissible value ('%s')." % (player.name, bid))
          sys.exit(2)

      player.bid = bid

      bid_sequence.append(bid)

      sum_of_bids += bid

      if create_learning_data:


        # Keeping track of the cards which could possibly be in enemies' hands:

        cards_left = [[], [], [], [], []]                                     # These lists will hold the cards of the respective color.

        for card in all_cards:
          cards_left[card.color].append(card)

        player.cards_left   = cards_left
        player.jesters_left = 4
        player.wizards_left = 4

        if trump_card != None:

          cards_left[trump_card.color].remove(trump_card)                     # The trump card cannot be in enemies' hands any more and thus it is removed from the corresponding list.

          if trump_card.number == 0:
            player.jesters_left -= 1

          if trump_card.number == 14:
            player.wizards_left -= 1

        for card in player.hand:

          cards_left[card.color].remove(card)                                 # The cards in the player's hand cannot be in enemies' hands and thus are removed from the list.

          if card.number == 0:
            player.jesters_left -= 1

          if card.number == 14:
            player.wizards_left -= 1

      player = player.successor


    if player.type == "h":

      bid = -1

      while ((bid not in range(round_no + 1)) or (bid == round_no - sum_of_bids)):

        display_last_trick(last_trick_display)
        display_bidding_phase_for_mc(no_of_players, round_no, round_starter, trump, trump_card, player, sum_of_bids)

        try:
          bid = int(input("%s, please enter your choice: " % player.name))
        except ValueError:
          bid = -1

    elif player.type == "s":

      bid = player.choose_bid(player.virtual_game, no_of_players, round_no, trump, trump_card, bid_sequence, player.hand, players, all_cards)

      if ((bid not in range(round_no + 1)) or (bid == round_no - sum_of_bids)):

        print("ERROR: The static algorithm %s returned an inadmissible value ('%s')." % (player.name, bid))
        sys.exit(2)

    elif player.type == "l":

      bid = player.choose_bid(player.virtual_game, no_of_players, round_no, trump, trump_card, bid_sequence, player.hand, players, all_cards)

      if ((bid not in range(round_no + 1)) or (bid == round_no - sum_of_bids)):

        print("ERROR: The learning agent %s returned an inadmissible value ('%s')." % (player.name, bid))
        sys.exit(2)

    player.bid = bid

    bid_sequence.append(bid)

    #print("")
    #print("These were the bids:")
    #for player in players:
      #print("%s: %d" % (player.name, player.bid))

    if create_learning_data:

      if trump_card != None:
        game_round.trump_card = Ccard(trump_card.color, trump_card.number)
      else:
        game_round.trump_card = None
      game_round.trump_color = trump

      for p in players:

        game_round.hands.append(list(p.hand))
        game_round.bids.append(p.bid)


      # Keeping track of the cards which could possibly be in enemies' hands:

      cards_left = [[], [], [], [], []]                                     # These lists will hold the cards of the respective color.

      for card in all_cards:
        cards_left[card.color].append(card)

      player.cards_left   = cards_left
      player.jesters_left = 4
      player.wizards_left = 4

      if trump_card != None:

        cards_left[trump_card.color].remove(trump_card)                     # The trump card cannot be in enemies' hands any more and thus it is removed from the corresponding list.

        if trump_card.number == 0:
          player.jesters_left -= 1

        if trump_card.number == 14:
          player.wizards_left -= 1

      for card in player.hand:

        cards_left[card.color].remove(card)                                 # The cards in the player's hand cannot be in enemies' hands and thus are removed from the list.

        if card.number == 0:
          player.jesters_left -= 1

        if card.number == 14:
          player.wizards_left -= 1


    # Trick-taking-phase:

    trick_starter = round_starter

    for trick_no in range(1, round_no + 1):     # Loop through the tricks

      #print("")
      #print("--- Round %s --- Trick %s ---" % (round_no, trick_no))

      del player_sequence[:]
      del card_sequence[:]

      trick = Ctrick()
      tricks.append(trick)


      # Loop through the players:

      player = trick_starter

      leading_color = -1                        # Possible leading colors: -1 = not yet set, 0 = wizard was played first, 1 = red, 2 = yellow, 3 = green, 4 = blue
      strongest_card_played = -1                # Later, a Ccard object will be linked here, representing the current strongest card of the trick

      for p in range(no_of_players):


        # Providing the playable cards:

        del playable_cards[:]

        hand = player.hand

        if leading_color <= 0:                  # If no no colorful card has been played this trick or if a wizard had been played before...

          playable_cards.extend(hand)           # Every card may be played

        else:                                   # If a colorful color (i.e. not a wizard) is the leading color...

          for card in hand:

            if card.color == leading_color:             # If the hand contains a card of leading color...

              for c in hand:                                            # Make only the cards of that color and the white cards playble

                if ((c.color == leading_color) or (c.color == 0)):
                  playable_cards.append(c)

              break

          if not playable_cards:                        # If no card of leading color has been found...
            playable_cards.extend(hand)                         # Every card may be played

        #print("%s can play one of these cards now:" % player.name)
        #display_cards(playable_cards)


        # Making the player choose a card to play:

        no_of_playable_cards = len(playable_cards)

        if player.type == "h":

          no_of_card_of_choice = -1

          while no_of_card_of_choice not in range(no_of_playable_cards):

            display_last_trick(last_trick_display)
            display_trick_taking_phase(no_of_players, round_no, trick_no, trick_starter, trump, trump_card, player, card_sequence, playable_cards)

            try:
              no_of_card_of_choice = int(input("%s, please enter your choice: " % player.name)) - 1
            except ValueError:
              no_of_card_of_choice = -1

        elif player.type == "s":

          no_of_card_of_choice = player.choose_card_to_play(player.virtual_game, no_of_players,
                                                            round_no, trick_no,
                                                            trump, trump_card,
                                                            mc_index, bid_sequence, player_sequence, card_sequence,
                                                            hand, playable_cards,
                                                            strongest_card_played, leading_color,
                                                            tricks, trick,
                                                            trick_starter, player, players,
                                                            len(deck))

          if no_of_card_of_choice not in range(no_of_playable_cards):

            print("ERROR: The static algorithm %s returned an inadmissible value." % player.name)
            sys.exit(2)

        elif player.type == "l":

          no_of_card_of_choice = player.choose_card_to_play(player.virtual_game, no_of_players,
                                                            round_no, trick_no,
                                                            trump, trump_card,
                                                            mc_index, bid_sequence, player_sequence, card_sequence,
                                                            hand, playable_cards,
                                                            strongest_card_played, leading_color,
                                                            tricks, trick,
                                                            trick_starter, player, players,
                                                            len(deck))

          if no_of_card_of_choice not in range(no_of_playable_cards):

            print("ERROR: The learning agent %s returned an inadmissible value." % player.name)
            sys.exit(2)

        #print("Played: %s (array index of card of choice)." % no_of_card_of_choice)


        # Gathering data:

        if create_learning_data:

          # Keeping track of the cards which could possibly be in enemies' hands:

          cards_left = player.cards_left

          if trick_no > 1:

            for card in tricks[trick_no - 2].card_sequence:

              if card in cards_left[card.color]:

                cards_left[card.color].remove(card)                             # This card has been played and thus it cannot be in enemies' hands and is removed from the list.

                if card.number == 0:
                  player.jesters_left -= 1

                if card.number == 14:
                  player.wizards_left -= 1

          for card in card_sequence:
            
            cards_left[card.color].remove(card)                                 # This card has been played and thus it cannot be in enemies' hands and is removed from the list.

            if card.number == 0:
              player.jesters_left -= 1

            if card.number == 14:
              player.wizards_left -= 1

          observation = gather_data(playable_cards[no_of_card_of_choice],
                                    no_of_players,
                                    round_no, trick_no,
                                    trump, trump_card,
                                    mc_index, bid_sequence, player_sequence, card_sequence,
                                    hand, playable_cards,
                                    strongest_card_played, leading_color,
                                    tricks, trick,
                                    trick_starter, player, players,
                                    len(deck),
                                    cards_left, player.jesters_left, player.wizards_left)

          game.data.append(observation)


        # Playing the card:

        card_played = playable_cards[no_of_card_of_choice]

        player_sequence.append(players.index(player))   # Write down the index of the current player
        card_sequence.append(card_played)               # Write down the card played

        hand.remove(card_played)

        if ((leading_color == -1) and (card_played.number > 0)) :       # Set the leading color, if it hasn't been set yet and a non-jester card has been played
          leading_color = card_played.color


        # Determine whether the played card is the new strongest card:

        if is_new_strongest_card(trump, leading_color, strongest_card_played, card_played):

          strongest_card_played = card_played
          trick_winner = player


        # The player's turn is done here.

        player = player.successor


      trick_winner.tricks += 1

      trick.player_sequence = list(player_sequence)
      trick.card_sequence   = list(card_sequence)
      trick.winner          = players.index(trick_winner)

      trick_starter = trick_winner

      if human_players_participating:

        last_trick_display = record_last_trick(player_sequence, card_sequence, round_no, trick_no, trump, trump_card, trick_winner, players)
        #display_last_trick(last_trick_display)

        if ((round_no == no_of_rounds) and (trick_no == round_no)):
          display_last_trick(last_trick_display)


    # Computation of the points:

    if human_players_participating:
      print("")
      print("Computation of points:           OLD + GAIN =   NEW")
      print("---------------------------------------------------")
    #print("")
    #print("Computation of points:           OLD + GAIN =   NEW")
    #print("---------------------------------------------------")

    for player in players:

      bid = player.bid
      won_tricks = player.tricks

      gain = 0

      if bid == won_tricks:

        gain = 20 + 10 * bid

      else:

        gain = -10 * abs(bid - won_tricks)

      old_points = player.points
      player.points += gain

      if human_players_participating:
        print("%-30s %5i + %4i = %5i" % (player.name + ":", old_points, gain, player.points))
      #print("%-30s %5i + %4i = %5i" % (player.name + ":", old_points, gain, player.points))

      if create_learning_data:

        game_round.won_tricks.append(won_tricks)
        game_round.gained_points.append(gain)


    # Preparations for the next loop:

    mc_index = (mc_index + 1) % no_of_players
    mc = mc.successor
    round_starter = round_starter.successor

  for p in range(no_of_players):
    game.final_points.append(players[p].points)

  print("")
  print("--- Game of %i players: ---" % no_of_players)
  for player in players:
    print("%s %-30s %5i" % (player.type, player.name, player.points))

  return game
