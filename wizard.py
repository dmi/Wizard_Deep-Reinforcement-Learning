import time
import sys
from random        import shuffle

from data          import *
from rating_system import *
from game          import *



def arg_rated(arg_index):

  if sys.argv[arg_index] in ["rated", "r"]:

    return True

  elif sys.argv[arg_index] in ["unrated", "u"]:

    return False

  else:

    print("ERROR: Unknown argument '%s' at position %i." % (sys.argv[arg_index], arg_index))
    sys.exit(1)


def arg_fixed_first_player(arg_index):

    if sys.argv[arg_index] in ["fixed", "f"]:

      return True

    elif sys.argv[arg_index] in ["not_fixed", "n"]:

      return False

    else:

      print("ERROR: Unknown argument '%s' at position %i." % (sys.argv[arg_index], arg_index))
      sys.exit(1)


def arg_no_of_players(arg_index):

    no_of_players = int(sys.argv[arg_index])

    if no_of_players not in [3, 4, 5, 6]:

      print("ERROR: The game must be played with 3, 4, 5 or 6 players.")
      sys.exit(1)

    else:

      return no_of_players


def set_up_players(start_arg_index, no_of_players):

  players = []

  i = start_arg_index                                                 # This counter is used in the following loop to work with sys.argv

  for p in range(no_of_players):

    if sys.argv[i] in ["human", "h"]:

      players.append(Cplayer(sys.argv[i], sys.argv[i + 1], 0, 0, 0, 0))                       # Adding a new human player to the game

    elif sys.argv[i] in ["static_algorithm", "s"]:

      source_file = sys.argv[i + 1]

      source_module_name = source_file[:-3].split('/')[-1]

      print("Source file name: %s" % source_file)
      print("Source module name: %s" % source_module_name)

      source_module = __import__(source_module_name)

      create_virtual_game = getattr(source_module, 'create_virtual_game')
      choose_trump        = getattr(source_module, 'choose_trump')
      choose_bid          = getattr(source_module, 'choose_bid')
      choose_card_to_play = getattr(source_module, 'choose_card_to_play')


      # Adding a new static playing algorithm to the game:

      players.append(Cplayer("s", source_module_name, create_virtual_game, choose_trump, choose_bid, choose_card_to_play))

    elif sys.argv[i] in ["learning_agent", "l"]:

      source_file         = sys.argv[i + 1]
      learning_agent_name = sys.argv[i + 2]

      source_module_name = source_file[:-3].split('/')[-1]

      print("Source file name: %s" % source_file)
      print("Source module name: %s" % source_module_name)

      source_module = __import__(source_module_name)

      create_virtual_game = getattr(source_module, 'create_virtual_game')
      choose_trump        = getattr(source_module, 'choose_trump')
      choose_bid          = getattr(source_module, 'choose_bid')
      choose_card_to_play = getattr(source_module, 'choose_card_to_play')

      player = Cplayer("l", learning_agent_name.split('/')[-1], create_virtual_game, choose_trump, choose_bid, choose_card_to_play)

      from neural_network import neural_network_load

      player.model, player.scalarX, player.scalarY = neural_network_load(learning_agent_name)

      # Adding a new learning agent to the game:

      players.append(player)

      i += 1

    else:

      print("ERROR: Unknown argument '%s' at position %i." % (sys.argv[i], i))
      sys.exit(1)

    i += 2

  return players


def main():

  print("")
  print("Welcome to Wizard!")
  print("==================")
  print("")

  sys.path.extend(["./static_algorithms", "./ml_algorithms", "./learning_agents", "./evoluted_models"])

  #print(len(sys.argv))
  #print(sys.argv)


  if sys.argv[1] in ["use", "u", "0"]:

    print("'use' is not yet implemented. This function is supposed to load parameters from files to improve the quickness of usability.")
    sys.exit(0)

  if sys.argv[1] in ["play", "p"]:

    # Invocation:
    # python3 wizard.py play rated/unrated create_learning_data/do_not_create_learning_data learning_output_file/0 #players type1 playername1 type2 playername2 ...

    rated = arg_rated(2)

    if sys.argv[3] in ["create_learning_data", "c"]:

      create_learning_data = True
      learning_output_file = sys.argv[4]

    elif sys.argv[3] in ["do_not_create_learning_data", "d"]:

      create_learning_data = False
      learning_output_file = "0"

      if sys.argv[4] != "0":

        print("WARNING: Learning data will not be created. Are you sure about argument at position 2?")

    else:

      print("ERROR: Unknown argument '%s' at position 3." % sys.argv[3])
      sys.exit(1)

    no_of_players = arg_no_of_players(5)
    players       = set_up_players(6, no_of_players)

    game = play(1, create_learning_data, no_of_players, players)

    games = [game]

    if create_learning_data:

      #display_data(game)
      write_data(games, learning_output_file)

    if rated:
      evaluate_list(games)

  elif sys.argv[1] in ["research", "r"]:

    # Invocation:
    # python3 wizard.py research #games learning_output_file #players type1 playername1 type2 playername2 ...
    
    rated = arg_rated(2)

    fixed_first_player   = arg_fixed_first_player(3)
    no_of_games          = int(sys.argv[4])
    learning_output_file = sys.argv[5]
    no_of_players        = arg_no_of_players(6)
    no_of_all_players    = arg_no_of_players(7)
    all_players          = set_up_players(8, no_of_all_players)

    if fixed_first_player:

      perm = [i for i in range(1, no_of_all_players)]

    else:

      perm = [i for i in range(no_of_all_players)]

    games = []

    for game_no in range(1, no_of_games + 1):

      shuffle(perm)

      if fixed_first_player:

        players = [all_players[0]] + [all_players[perm[i]] for i in range(1, no_of_players)]

      else:

        players = [all_players[perm[i]] for i in range(no_of_players)]

      shuffle(players)

      games.append(play(game_no, True, no_of_players, players))

    write_data(games, learning_output_file)

    if rated:
        evaluate_list(games)

  elif sys.argv[1] in ["teach", "t"]:

    # Invocation:
    # python3 wizard.py teach learning_data_file learning_agent_name

    learning_data_file  = sys.argv[2]
    learning_agent_name = sys.argv[3]

    from neural_network import neural_network_teach

    neural_network_teach(learning_data_file, learning_agent_name)

  elif sys.argv[1] in ["evolve", "e"]:

    learning_data_file      = sys.argv[2]
    learning_agent_old_name = sys.argv[3]
    learning_agent_new_name = sys.argv[4]

    from neural_network import neural_network_load, neural_network_evolve

    model, scalarX, scalarY = neural_network_load(learning_agent_old_name)

    neural_network_evolve(model, scalarX, scalarY, learning_data_file, learning_agent_new_name)

  elif sys.argv[1] in ["evaluate"]:

    # Invocation:
    # python3 wizard.py evaluate game_results_file

    game_results_file = sys.argv[2]

    evaluate_games_from_file(game_results_file)

  elif sys.argv[1] in ["compare"]:

    # Invocation:
    # python3 wizard.py compare #games #players type1 playername1 type2 playername2 ...

    desired_no_of_games = int(sys.argv[2])
    no_of_players       = int(sys.argv[3])

    players = set_up_players(4, no_of_players)

    compare_players(desired_no_of_games, no_of_players, players)

  return 0


main()
