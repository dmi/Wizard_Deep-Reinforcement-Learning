rm rating_list.txt
python3 wizard.py compare 100 3 \
s static_algorithms/tendency_bot_alpha_8.py \
s static_algorithms/tendency_bot_alpha_6.py \
l ml_algorithms/neural_network_autonom.py cached_models/multi_learner && \
cat rating_list_sorted_by_avg_points.txt

rm rating_list.txt
python3 wizard.py compare 100 3 \
s static_algorithms/tendency_bot_alpha_8.py \
s uniform_random_bot.py \
l ml_algorithms/neural_network_autonom.py featured_softmax_bot && \
cat rating_list_sorted_by_avg_points.txt
