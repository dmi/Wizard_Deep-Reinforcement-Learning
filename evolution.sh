#!/bin/sh
set -x

research_games=3000
evolution_games=300

clear; clear; clear

rm coached_learning_data.csv

python3 wizard.py research rated not_fixed $research_games coached_learning_data.csv 3 4 \
s static_algorithms/tendency_bot_alpha_8.py \
s static_algorithms/uniform_random_bot.py \
s static_algorithms/greedy_bot.py \
s static_algorithms/tendency_bot_alpha_6.py && \
cp rating_list_sorted_by_avg_points.txt rating_list_$1.txt && \
python3 wizard.py teach coached_learning_data.csv $1_0

rm rating_list.txt ; rm rating_list_s*

trap "exit" INT

i=0

while : #for i in $(seq 1 $2)
do

  i=$(($i+1))
  j=$(($i-1))

  rm coached_learning_data.csv

  python3 wizard.py research rated fixed $evolution_games coached_learning_data.csv 3 5 \
  l ml_algorithms/neural_bid_bot.py $1_$j \
  s static_algorithms/tendency_bot_alpha_8.py \
  s static_algorithms/uniform_random_bot.py \
  s static_algorithms/greedy_bot.py \
  s static_algorithms/tendency_bot_alpha_6.py && \
  cat rating_list_sorted_by_avg_points.txt >> rating_list_$1.txt && \
  python3 wizard.py evolve coached_learning_data.csv $1_$j $1_$i

  rm rating_list.txt ; rm rating_list_s*

done
