#!/bin/sh
set -x      # With this line, all commands of this script will be written on the screen before being executed.
#set -e      # With this line, the whole script will exit as soon as one command creates an error.


learning_agent_name=NN_7_200k
research_games=200000
learning_data_file=/scratch/daniel.missal/coached_learning_data_200k.csv


clear; clear; clear

rm $learning_data_file

python3 wizard.py research $research_games $learning_data_file 3 4 \
s static_algorithms/tendency_bot_alpha_8.py \
s static_algorithms/uniform_random_bot.py \
s static_algorithms/greedy_bot.py \
s static_algorithms/tendency_bot_alpha_6.py

if [ $? != 0 ]; then exit 1; fi

python3 wizard.py teach $learning_data_file $learning_agent_name

if [ $? != 0 ]; then exit 1; fi

rm rating_list.txt ; rm rating_list_s*

python3 wizard.py compare 100 3 \
s static_algorithms/tendency_bot_alpha_8.py \
s uniform_random_bot.py \
l ml_algorithms/neural_network_autonom.py $learning_agent_name

if [ $? != 0 ]; then exit 1; fi

cat rating_list_sorted_by_avg_points.txt
