from random import randint
from game import *



class Cvirtual_game:
  
  def __init__(self, no_of_players, assigned_player_no):
    
    self.no_of_players = no_of_players
    self.assigned_player_no = assigned_player_no


def create_virtual_game(no_of_players, assigned_player_no):
  
  #print("")
  #print("tendency_bot: Registering a new game of % s players with the assigned player number %s." % (no_of_players, assigned_player_no))
  
  virtual_game = Cvirtual_game(no_of_players, assigned_player_no)
  
  return virtual_game


def choose_trump(virtual_game, no_of_players, round_no, hand):
  
  #print("")
  #print("tendency_bot: Choosing a trump color.")
  
  cards_of_color = [0, 0, 0, 0, 0]                              # White, red, yellow, green, blue
  sum_of_numbers_of_color = [0, 0, 0, 0, 0]                     # 
  #mean_number_of_color = [0, 0, 0, 0, 0]                        # 
  rating_of_color = [0, 0, 0, 0, 0]                             # 
  
  for card in hand:
    
    color = card.color
    
    cards_of_color[color] += 1
    sum_of_numbers_of_color[color] += card.number
  
  for color in range(0, 5):
    #mean_number_of_color[color] = float(sum_of_numbers_of_color[color] / cards_of_color[color])
    rating_of_color[color] = float(cards_of_color[color] * 100 + sum_of_numbers_of_color[color])
  
  highest_rated_color = -1
  highest_rating = -1
  
  for color in range(1, 5):
    
    if rating_of_color[color] > highest_rating:
      
      highest_rated_color = color
      highest_rating = rating_of_color[color]
  
  trump = highest_rated_color
  
  #display_cards(hand)
  #print("Rating of colors:")
  #print(rating_of_color)
  #print("Choice:")
  #print(trump)
  #input("Press enter to continue!")
  
  return trump


def choose_bid(virtual_game, no_of_players, round_no, trump, trump_card, bid_sequence, hand, players, all_cards):
  
  #print("")
  #print("tendency_bot: Choosing a bid.")
  
  avg_part = float(round_no / no_of_players)
  
  bid = 0
  
  for card in hand:
    if ((card.number >= 10) or ((trump > 0) and (card.color == trump))):
      bid += 1
  
  bid -= 1
  
  if bid < 0:
    bid = 0
  
  if len(bid_sequence) == (virtual_game.no_of_players - 1):
    
    if bid == (round_no - sum(bid_sequence)):
      
      #print("Illegal bid = %i" % bid)
      #print("avg_part = %f" % avg_part)
      
      if avg_part < bid:
        bid -= 1
      else:
        bid += 1
  
  #display_cards(hand)
  #print("trump = %i" % trump)
  #print("Choice:")
  #print(bid)
  #input("Press enter to continue!")
  
  return bid


def choose_card_to_play(virtual_game, no_of_players,
                        round_no, trick_no,
                        trump, trump_card,
                        mc_index, bid_sequence, player_sequence, card_sequence,
                        hand, playable_cards,
                        strongest_card_played, leading_color,
                        tricks, trick,
                        trick_starter, player, players,
                        deck_size):
  
  #print("")
  #print("tendency_bot: Choosing a card to play")

  index_of_choice = randint(0, len(playable_cards) - 1)
  
  return index_of_choice
