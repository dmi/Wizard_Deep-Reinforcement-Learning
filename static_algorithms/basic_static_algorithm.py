class Cvirtual_game:
  
  def __init__(self, no_of_players, assigned_player_no):
    
    self.no_of_players = no_of_players
    self.assigned_player_no = assigned_player_no


def create_virtual_game(no_of_players, assigned_player_no):
  
  #print("")
  #print("basic_static_algorithm: Registering a new game of % s players with the assigned player number %s." % (no_of_players, assigned_player_no))
  
  virtual_game = Cvirtual_game(no_of_players, assigned_player_no)
  
  return virtual_game


def choose_trump(virtual_game, no_of_players, round_no, hand):
  
  #print("")
  #print("basic_static_algorithm: Choosing a trump color.")
  
  trump = 1     # This is the return value of the function. It must be an integer of the set {1, 2, 3, 4} which corresponds to {red, yellow, green, blue}.
                # In this basic example, the static algorithm always chooses red to be the trump.
  
  return trump


def choose_bid(virtual_game, no_of_players, round_no, trump, trump_card, bid_sequence, hand, players, all_cards):
  
  #print("")
  #print("basic_static_algorithm: Choosing a bid.")
  
  bid = 0       # This is the return value of the function. It must be an integer of the set {0, 1, ... , round_no} and incase the bid_sequence has
                # (no_of_players - 1) entries, it must not be (round_no - "the sum of all entries in bid_sequence").
                # In this basic example, the static algorithm always chooses to bid 0, if possible, otherwise 1.
  
  if len(bid_sequence) == (virtual_game.no_of_players - 1):
    
    if bid == (round_no - sum(bid_sequence)):
      
      bid = 1
  
  return bid


def choose_card_to_play(virtual_game, no_of_players,
                        round_no, trick_no,
                        trump, trump_card,
                        mc_index, bid_sequence, player_sequence, card_sequence,
                        hand, playable_cards,
                        strongest_card_played, leading_color,
                        tricks, trick,
                        trick_starter, player, players,
                        deck_size):
  
  #print("")
  #print("basic_static_algorithm: Choosing a card to play")

  index_of_choice = 0       # This is the return value of the function. It must be an integer of the set {0, 1, ... , len(playable_cards) - 1}.
                            # In this basic example, the static algorithm always chooses the index of the first element of the list of all playable cards.
  
  return index_of_choice
