from math import factorial as fac
from random import randint
from game import *



class Cvirtual_game:

  def __init__(self, no_of_players, assigned_player_no):

    self.no_of_players = no_of_players
    self.assigned_player_no = assigned_player_no


def create_virtual_game(no_of_players, assigned_player_no):

  #print("")
  #print("tendency_bot: Registering a new game of % s players with the assigned player number %s." % (no_of_players, assigned_player_no))

  virtual_game = Cvirtual_game(no_of_players, assigned_player_no)

  return virtual_game


def choose_trump(virtual_game, no_of_players, round_no, hand):

  #print("")
  #print("tendency_bot: Choosing a trump color.")

  cards_of_color = [0, 0, 0, 0, 0]                              # White, red, yellow, green, blue
  sum_of_numbers_of_color = [0, 0, 0, 0, 0]                     #
  #mean_number_of_color = [0, 0, 0, 0, 0]                        #
  rating_of_color = [0, 0, 0, 0, 0]                             #

  for card in hand:

    color = card.color

    cards_of_color[color] += 1
    sum_of_numbers_of_color[color] += card.number

  for color in range(0, 5):
    #mean_number_of_color[color] = float(sum_of_numbers_of_color[color] / cards_of_color[color])
    rating_of_color[color] = float(cards_of_color[color] * 100 + sum_of_numbers_of_color[color])

  highest_rated_color = -1
  highest_rating = -1

  for color in range(1, 5):

    if rating_of_color[color] > highest_rating:

      highest_rated_color = color
      highest_rating = rating_of_color[color]

  trump = highest_rated_color

  #display_cards(hand)
  #print("Rating of colors:")
  #print(rating_of_color)
  #print("Choice:")
  #print(trump)
  #input("Press enter to continue!")

  return trump


def choose_bid(virtual_game, no_of_players, round_no, trump, trump_card, bid_sequence, hand, players, all_cards):

  #print("")
  #print("tendency_bot: Choosing a bid.")


  # Keeping track of the cards which could possibly be in enemies' hands:

  cards_left = [[], [], [], [], []]                                     # These lists will hold the cards of the respective color.

  for card in all_cards:
    cards_left[card.color].append(card)

  virtual_game.cards_left = cards_left
  virtual_game.jesters_left = 4
  virtual_game.wizards_left = 4

  if trump_card != None:

    cards_left[trump_card.color].remove(trump_card)                     # The trump card cannot be in enemies' hands any more and thus it is removed from the corresponding list.

    if trump_card.number == 0:
      virtual_game.jesters_left -= 1

    if trump_card.number == 14:
      virtual_game.wizards_left -= 1

  for card in hand:

    cards_left[card.color].remove(card)                                 # The cards in the player's hand cannot be in enemies' hands and thus are removed from the list.

    if card.number == 0:
      virtual_game.jesters_left -= 1

    if card.number == 14:
      virtual_game.wizards_left -= 1


  # Choosing a bid:

  avg_part = float(round_no / no_of_players)

  bid = 0

  for card in hand:
    if ((card.number >= 10) or ((trump > 0) and (card.color == trump))):
      bid += 1

  bid -= 1

  if bid < 0:
    bid = 0

  if len(bid_sequence) == (virtual_game.no_of_players - 1):

    if bid == (round_no - sum(bid_sequence)):

      #print("Illegal bid = %i" % bid)
      #print("avg_part = %f" % avg_part)

      if avg_part < bid:
        bid -= 1
      else:
        bid += 1

  #display_cards(hand)
  #print("trump = %i" % trump)
  #print("Choice:")
  #print(bid)
  #input("Press enter to continue!")

  return bid


def superiority_probability(virtual_game, no_of_players, round_no, trick_no, deck_size, card_sequence, trump, leading_color, strongest_card_played, card):

  if not is_new_strongest_card(trump, leading_color, strongest_card_played, card):
    return 0

  number = card.number

  if number == 14:
    return 1

  no_of_successors = no_of_players - len(card_sequence) - 1

  if no_of_successors == 0:
    return 1

  hand_size = round_no - trick_no + 1

  r = no_of_successors * hand_size                      # no_of_cards_in_successor_hands

  N = deck_size + r                                     # no_of_unknown_cards

  S = 0                                                 # no_of_stronger_unknown_cards

  if number == 0:

    S = N - virtual_game.jesters_left

  else:                                                 # At this point, there must be a leading color and card must be of leading color or a trump card.

    S = virtual_game.wizards_left

    cards_left = virtual_game.cards_left

    color = card.color

    for unknown_card in cards_left[color]:

      if unknown_card.number > number:

        S += 1

    if ((trump > 0) and (color != trump)):

      for unknown_card in cards_left[trump]:

        S += 1

  W = N - S                                             # no_of_weaker_unknown_cards

  if N - r - S < 0:                                     # In this situation, there is always an enemy who has at least one stronger card in his hand.
    return 0

  return float(fac(W) * fac(N - r) / fac(N) / fac(N - r - S))


def rate_move(card):

  return 0


def choose_card_to_play(virtual_game, no_of_players,
                        round_no, trick_no,
                        trump, trump_card,
                        mc_index, bid_sequence, player_sequence, card_sequence,
                        hand, playable_cards,
                        strongest_card_played, leading_color,
                        tricks, trick,
                        trick_starter, player, players,
                        deck_size):

  #print("")
  #print("tendency_bot: Choosing a card to play")

  #display_trick_taking_phase(no_of_players, round_no, trick_no, trick_starter, trump, trump_card, player, card_sequence, playable_cards)


  # Keeping track of the cards which could possibly be in enemies' hands:

  cards_left = virtual_game.cards_left

  if trick_no > 1:

    for card in tricks[trick_no - 2].card_sequence:

      if card in cards_left[card.color]:

        cards_left[card.color].remove(card)                             # This card has been played and thus it cannot be in enemies' hands and is removed from the list.

        if card.number == 0:
          virtual_game.jesters_left -= 1

        if card.number == 14:
          virtual_game.wizards_left -= 1

  for card in card_sequence:

    cards_left[card.color].remove(card)                                 # This card has been played and thus it cannot be in enemies' hands and is removed from the list.

    if card.number == 0:
      virtual_game.jesters_left -= 1

    if card.number == 14:
      virtual_game.wizards_left -= 1

  
  # Choosing a card to play:

  move_ratings = []                                                     # Stores a rating for each possible move
  superiority_probabilities = []

  for card in playable_cards:
    superiority_probabilities.append(superiority_probability(virtual_game,
                                                             no_of_players, round_no, trick_no,
                                                             deck_size, card_sequence, trump, leading_color, strongest_card_played, card))

  best_rating = -100000000000
  best_move_index = 0

  move_index = 0

  for card in playable_cards:

    #rating = rate_move(card)
    rating = superiority_probabilities[move_index] # ATTENTION: Change it back to the upper line again!

    move_ratings.append(rating)

    #s = "Rating of option %2i" % move_index + " (Card " + card.symbol()
    #if card.number not in [10, 11, 12, 13]:
      #s += " "
    #s += ") is %8.3f." % rating
    #print(s)

    if rating > best_rating:

      best_move_index = move_index
      best_rating = rating

    move_index += 1

  index_of_choice = best_move_index

  #print("")
  #input("Index of choice is %i. Press enter to continue!" % index_of_choice)

  return index_of_choice
