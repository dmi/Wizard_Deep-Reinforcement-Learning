import joblib
import time
import os.path



def neural_network_teach(learning_data_file, learning_agent_name):

    import pandas
    import numpy
    from keras.models                import Sequential
    from keras.layers                import Dense, SimpleRNN
    from keras.wrappers.scikit_learn import KerasRegressor
    from sklearn.preprocessing       import MinMaxScaler

    dataframe = pandas.read_csv(learning_data_file, header=None)
    dataset   = dataframe.values
    # numpy.random.shuffle(dataset)

    no_of_features               = len(dataframe.columns) - 1
    no_of_1st_hidden_layer_nodes = 80
    no_of_2nd_hidden_layer_nodes = 40
    no_of_3rd_hidden_layer_nodes = 20
    no_of_4th_hidden_layer_nodes = 10
    no_of_epochs                 =  3

    model = Sequential()
    model.add(Dense(no_of_1st_hidden_layer_nodes, kernel_initializer = 'normal', activation = 'relu', input_dim = no_of_features))
    model.add(Dense(no_of_2nd_hidden_layer_nodes, kernel_initializer = 'normal', activation = 'relu'))
    model.add(Dense(no_of_3rd_hidden_layer_nodes, kernel_initializer = 'normal', activation = 'relu'))
    model.add(Dense(no_of_4th_hidden_layer_nodes, kernel_initializer = 'normal', activation = 'relu'))
    model.add(Dense(1,                            kernel_initializer = 'normal'))
    model.compile(loss = 'mean_squared_error', optimizer = 'adam')

    X = dataset[:,0:no_of_features]
    Y = dataset[:,no_of_features]

    scalarX, scalarY = MinMaxScaler(), MinMaxScaler()
    scalarX.fit(X)
    scalarY.fit(Y.reshape(len(Y), 1))

    X = scalarX.transform(X)
    Y = scalarY.transform(Y.reshape(len(Y), 1))

    model.fit(X, Y, epochs = no_of_epochs, verbose = 1)

    model.save(          learning_agent_name + '_model.h5')
    joblib.dump(scalarX, learning_agent_name + '_scalarx.h5')
    joblib.dump(scalarY, learning_agent_name + '_scalary.h5')


def neural_network_load(learning_agent_name):

    print("Time:")
    time_before = time.time()
    print(time_before)

    print("Importing libraries...")
    from keras.models      import load_model
    print("... finished importing libraries.")

    print("Loading model ...")
    if not os.path.exists(learning_agent_name + '_model.h5'):
      learning_agent_name = 'learning_agents/' + learning_agent_name
    model   =  load_model(learning_agent_name + '_model.h5')
    scalarX = joblib.load(learning_agent_name + '_scalarx.h5')
    scalarY = joblib.load(learning_agent_name + '_scalary.h5')
    print("... finished loading model.")

    print("Time:")
    time_after = time.time()
    print(time_after)
    print("Time-Diff:")
    print(time_after - time_before)

    return model, scalarX, scalarY


def neural_network_evolve(model, scalarX, scalarY, learning_data_file, learning_agent_name):

    import pandas
    from keras.models                import Sequential
    from keras.layers                import Dense
    from keras.wrappers.scikit_learn import KerasRegressor
    from sklearn.preprocessing       import MinMaxScaler

    dataframe = pandas.read_csv(learning_data_file, header=None)
    dataset   = dataframe.values

    no_of_features               = len(dataframe.columns) - 1
    no_of_epochs                 = 3

    X = dataset[:,0:no_of_features]
    Y = dataset[:,no_of_features]

    scalarX, scalarY = MinMaxScaler(), MinMaxScaler()
    scalarX.fit(X)
    scalarY.fit(Y.reshape(len(Y), 1))

    X = scalarX.transform(X)
    Y = scalarY.transform(Y.reshape(len(Y), 1))

    model.fit(X, Y, epochs = no_of_epochs, verbose = 1)

    model.save(          learning_agent_name + '_model.h5')
    joblib.dump(scalarX, learning_agent_name + '_scalarx.h5')
    joblib.dump(scalarY, learning_agent_name + '_scalary.h5')
